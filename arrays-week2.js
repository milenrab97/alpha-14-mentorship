const matrix1 = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]
// 1) Write a function which swaps 2 elements in a matrix 
// can you do it without mutatating the initial matrix?

const numbers = [0, 1, 2, 3, 4, 5, 6, 7]
// 2) Write a function which inserts an element in a given array after the afterIndex position
// insertAt(3, 9, numbers) to return [0, 1, 2, 3, 9, 4, 5, 6, 7]
const insert = (afterIndex, element, array) => {}

// 3) similar to 2) but a function to move an element at fromIndex after the afterIndex position
//  move(1, 4, numbers) to return [0, 2, 3, 4, 1, 5, 6, 7]
const move = (fromIndex, afterIndex, array) => {}

// What's the best time complexity for 2) and 3) given that the array is sorted?