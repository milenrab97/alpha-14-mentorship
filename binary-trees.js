// ----------------------------------------------------------
const values1 = [1, 2, 3, 4, 5, 6, 7, 8]
const sorted1 = [5, 3, 7, 2, 4, 6, 8, 1] // -> will be printed in order from 1 to 8 with preorder traversal

// function to construct a tree from array of values level by level from left to right
const constructTree = values => {
  let root = null

  const addNodeLevelWise = value => {
    const newNode = {
      value,
      left: null,
      right: null
    }

    if (!root) {
      root = newNode
      return true
    }

    let nodeToAddTo = root
    const q = [nodeToAddTo]

    while (q.length > 0) {
      nodeToAddTo = q.shift()
      if (!nodeToAddTo.left) {
        nodeToAddTo.left = newNode
        break
      }
      q.push(nodeToAddTo.left)
      if (!nodeToAddTo.right) {
        nodeToAddTo.right = newNode
        break
      }
      q.push(nodeToAddTo.right)
    }
  }

  values.forEach(value => {
    addNodeLevelWise(value)
  })

  return root
}

const preOrder = node => {
  if (!node) {
    return
  }

  preOrder(node.left)
  console.log(node.value)
  preOrder(node.right)
}

// e.g.

const root1 = constructTree(sorted1)
console.log("sorted1", sorted1)
preOrder(root1)

// ----------------------------------------------------------

const tasksTreeRoot = constructTree([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
// Task 1: Write a function which calculates the sum of the leaf nodes with even values -> 30 from the tree above

// Task 2: Write a function which accepts a root and a value and prints the path to that value from the root.
// for example, the path to 6 from the tree above is: 1 3 6
