// 1) Write a function which accepts a number N and returns as follows:
// N: 2 -> 121
// N: 4 -> 121312141213121

const randomStructure = [
  {
    name: "A",
    children: [
      {
        name: "A1",
        children: [
          {
            name: "A11",
            sold: true
          },
          {
            name: "A12",
            sold: false
          }
        ]
      },
      {
        name: "A2",
        children: [
          {
            name: "A21",
            sold: true
          }
        ]
      }
    ]
  },
  { name: "B", children: [] }
];

// 2) Write a recursive function that returns the leaf nodes of randomStructure
// (the expected result is an array of objects with names A11, A12, A21)

const randomStructure1 = [randomStructure[0], { name: "B" }];
// 3) Same as 2) but make sure that it works for the array randomStructure1

// 4) Same as above but in the recursion filter the leaf nodes that are sold only

// 5) Same as 4) but return only their names
