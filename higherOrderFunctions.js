/* eslint-disable no-unused-vars */
const users = [
    {
        name: 'A',
        age: 20,
        isAdmin: false,
    },
    {
        name: 'B',
        age: 15,
        isAdmin: true,
    },
    {
        name: 'C',
        age: 35,
        isAdmin: false,
    },
    {
        name: 'D',
        age: 61,
        isAdmin: true,
    },
    {
        name: 'E',
        age: 61,
        isAdmin: true,
    },
]

// 1) remove the age property
const res1 = users.map(({ age, ...user }) => ({
    ...user,
}))

// 2) add property isAdminUnder18 (boolean)
const res2 = users.map(user => ({
    ...user,
    isAdminUnder18: user.isAdmin && user.age < 18,
}))

// 3) add property isAdminUnder18: true only if (isAdmin === true && age < 18) - else leave the object unchanged
const res3 = users.map(user => ({
    ...user,
    ...(user.isAdmin && user.age < 18 ? { isAdminUnder18: true } : {}),
}))

// -----

// 4) get only the users that are selected
const selectedUserNames = ['B', 'C', 'D', 'E']
const res4 = users.filter(user => selectedUserNames.includes(user.name))

// 5) get the sum of the ages of the admins that are selected
const res5 = users
    .filter(user => selectedUserNames.includes(user.name) && user.isAdmin)
    .map(({ age }) => age)
    .reduce((totalAge, age) => totalAge + age, 0)

// way 2 with point free and currying
const getProp = prop => obj => obj[prop]
const sum = (a, b) => a + b
const selectedUsers = users.filter(user => selectedUserNames.includes(user.name)) // later, we can do this better
const rest5w2 = selectedUsers
    .filter(getProp('isAdmin'))
    .map(getProp('age'))
    .reduce(sum, 0)

// 6) if the sum from 5) is greater than 75 -> return it. else return an array of the ages
const res6 = users
    .filter(user => selectedUserNames.includes(user.name) && user.isAdmin)
    .map(({ age }) => age)
    .reduce((totalAge, age, index, src) => (index === src.length - 1 && totalAge + age <= 75 ? src : totalAge + age), 0)

// 7) same as 6) but return an array without duplicates ([15, 61] instead of [15, 61, 61])
const res7 = users
    .filter(user => selectedUserNames.includes(user.name) && user.isAdmin)
    .map(({ age }) => age)
    .reduce(
        (totalAge, age, index, src) =>
            index === src.length - 1 && totalAge + age <= 150 ? [...new Set(src)] : totalAge + age,
        0
    )

// 8) same as 6) but return an array of the selected admins instead of their ages

// 9) same as 8) but return an array containing only admins with different ages (if there is already an admin of age 61, you cannot add another)

// 10) groupBy the users by age. expected format: { 61: [{ // user3 //}, {// user4 //}],  20: [{// user1 //}], ...}

console.log('res7', res7)